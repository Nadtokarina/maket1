let btn = document.querySelector(".delete");

const r = {
    "first":6,
    "second":37,
    "operation":"+",
    "expression":"6 + 37",    
    "answer":43,
}

document.querySelector('.card').innerHTML = `<table class="math"></table>`;
for (key in r) {
    let name = document.createElement('tr');
    name.innerHTML = `<td colspan='2' class="name">${key}: </td>`;
    document.querySelector('.math').appendChild(name);
    
    let meaning = document.createElement('tr');
    meaning.innerHTML = `<td class="index">${r[key]}</td>`;
    document.querySelector('.math').appendChild(meaning);

}

btn.addEventListener('click', function() {
    delete r.answer;
})

